# Font Icons

### Usage
CDN:
```html
<link rel="stylesheet" media="all" href="//unpkg.com/conwy-mind-icons@1.0.1/dist/i.css" />
```

### Credit
Majority of these icons I did not create so huge credit to https://www.mind.org.uk UK charity supporting mental health.

I do not represent Mind or Conwy Mind.

### For other local minds
Change svg/local.svg to your own, I recommend keeping positioning same as me as it lines up perfectly with the Mind logo.

### Requirements
- Node 10
- NPM or Yarn

### Build Instructions
```bash
npm install
npm run build
```

### Contributing
Please do a pull request.
- SVG's only
- 1000px height

### Requests
Feel free to request icons and I'll see what I can do, post a "issue" for requests.
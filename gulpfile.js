const { src, dest } = require('gulp');
const imagemin = 		require("gulp-imagemin");
const iconfont = 		require("gulp-iconfont");
const iconfontCss = 	require('gulp-iconfont-css');
const iconfontHtml = 	require('./modules/gulp-iconfont-html');


function iconTask(cb) {

	var fontName = 'mind-icons';

	src('svg/*.svg')
        // .pipe(imagemin([
        //     imagemin.svgo({
        //         plugins: [
		// 			{cleanupNumericValues: 0},
		// 			{removeViewBox: true},
		// 			{cleanupIDs: true}
        //         ]
        //     })
        // ]))
		.pipe(iconfontHtml({
			path: '.icons.template.html',
			targetPath: 'public/index.html'
		}))
		.pipe(iconfontCss({
			fontName: fontName,
			path: '.icons.template.css',
			targetPath: '../css/icons.css',
			fontPath: '../fonts/',
			cacheBuster: Math.round(+new Date / 1e3)
		}))
		.pipe(iconfont({
			  fontName: fontName
		}))
        .pipe(dest('public/fonts'))
        .on('finish', cb);

}

function distTask(cb) {

	var fontName = 'mind-icons';

	src('svg/*.svg')
        // .pipe(imagemin([
        //     imagemin.svgo({
        //         plugins: [
		// 			{cleanupNumericValues: 0},
		// 			{removeViewBox: true},
		// 			{cleanupIDs: true}
        //         ]
        //     })
        // ]))
		.pipe(iconfontCss({
			fontName: fontName,
			path: '.icons.template.css',
			targetPath: '../i.css',
			fontPath: 'fonts/'
		}))
		.pipe(iconfont({
			  fontName: fontName
		}))
        .pipe(dest('dist/fonts'))
        .on('finish', cb);

}

/* EXPORTS */
exports.dist = distTask;
exports.default = iconTask;
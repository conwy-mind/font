'use strict';

const 
	through = require('through2'),
	path = require('path'),
	consolidate = require('consolidate'),
	//app = require('./package.json');
	app = { name: 'gulp-iconfont-html' };


function error (txt, cb) {
	console.error(app.name + ': ' + txt);
	return cb();
}

function iconfontHTML (config) {

	config.path = config.path || 'html';
	config.targetPath = config.targetPath || '_icons.html';
	config.engine = config.engine || 'lodash';
	config.cssClass = config.cssClass || 'icon';
	
	let glyphs = [];

	function bufferContents (file, enc, cb) {

		if (file.isNull())
			return cb();

		if (file.isStream())
			return error('Streaming not supported', cb);


		const fileName = path.basename(file.path, '.svg');

		glyphs.push(fileName);

		this.push(file);
		cb();
	};

	function flush (cb) {
		const pass = {
			glyphs: glyphs,
			cssClass: config.cssClass
		};

		consolidate[config.engine](config.path, pass, function (err, html) {
				if ( err ) return error('Error in template: ' + err.message, cb);

				require('fs').writeFile(config.targetPath, html, cb);
		});

	}


	return through.obj(bufferContents, flush);
};


module.exports = iconfontHTML;
